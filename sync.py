#!/usr/bin/python

import json
import sys
from lxml import etree
from vtiger import get_challenge_token
from vtiger import get_session
from vtiger import get_company_id
from vtiger import get_opps
from vtiger import list_opps
from vtiger import delete_opps
from vtiger import parse_and_create_opps
from capsule import get_open_opps
from capsule import get_closed_opps
from config import company_name
from config import user_name
from config import user_access_key
from config import vtiger_url
from config import capsule_url
from config import capsule_token

# check config
if user_name == "":
    print "Dude, check config.py missing VTiger user name!"
    sys.exit(0)
if user_access_key == "":
    print "Dude, check config.py missing VTiger user access key!"
    sys.exit(0)
if vtiger_url == "":
    print "Dude, check config.py missing VTiger URL!"
    sys.exit(0)
if company_name == "":
    print "Dude, check config.py missing VTiger company name!"
    sys.exit(0)
if capsule_url == "":
    print "Dude, check config.py missing Capsule URL!"
    sys.exit(0)
if capsule_token == "" or capsule_token == ":x":
    print "Dude, check config.py missing Capsule token!"
    sys.exit(0)

print "Sync from " + capsule_url + " to " + vtiger_url

# get vtiger session_name and user_id
session = get_session(get_challenge_token())

# determine id of the vtiger company which contains the opps
company_id = get_company_id(session)

# get all vtiger opps for company_id
result_vtiger_opps = get_opps(session, company_id)
parsed_json = json.loads(result_vtiger_opps)
vtiger_opps = parsed_json["result"]

# get all open capsule opps
open_capsule_opps = get_open_opps()

# get all closed opps in last 90 days
closed_capsule_opps = get_closed_opps()

# check for cron option
if len(sys.argv) > 1:
    if sys.argv[1] == "cron":
        if len(vtiger_opps) < 1:
            print "Found no opps for " + company_name
        else:
            print "Delete all vtiger opps for " + company_name + "..."
            delete_opps(session, result_vtiger_opps)

        print "Import all open capsule opps ..."
        parse_and_create_opps(open_capsule_opps, company_id, session)
        print "Import closed capsule opps ..."
        parse_and_create_opps(closed_capsule_opps, company_id, session)
        sys.exit(0)

while True:
    print "\n1. List all vtiger opps for " + company_name
    print "2. Delete all vtiger opps for " + company_name
    print "3. Import all open capsule opps into vtiger"
    print "4. Import closed capsule opps into vtiger"
    print "q for quit\n"
    user_input = raw_input("Your choice:")

    if user_input == "1":
        if len(vtiger_opps) < 1:
            print "Found no opps for " + company_name
        else:
            list_opps(result_vtiger_opps)
    elif user_input == "2":
        if len(vtiger_opps) < 1:
            print "Found no opps for " + company_name
        else:
            delete_opps(session, result_vtiger_opps)
    elif user_input == "3":
        parse_and_create_opps(open_capsule_opps, company_id, session)
    elif user_input == "4":
        parse_and_create_opps(closed_capsule_opps, company_id, session)
    elif user_input == "q":
        print "Bye!"
        sys.exit(0)
